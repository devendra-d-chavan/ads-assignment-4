CSC 541: Assignment #4 - External Searching
==============================
The goals of this assignment is to study different methods of external 
searching. We will do this by comparing an algorithm designed for in-memory 
efficiency (BST) with an algorithm specifically designed for external searching 
(B-Tree).

Development environment
------------------------------ 
Visual Studio 2012 Ultimate on Windows 7 Home Premium 64 bit

Testing
------------------------------ 
 * Executables Tested with Pro Engineer Wildfire 5.0 (Win 7 32bit) VCL image 
 * Generated files for the test inputs present in source/Outputs
