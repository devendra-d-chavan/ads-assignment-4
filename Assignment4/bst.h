#include "filereader.h"

typedef struct BSTNode {
	int key;     // Key value
	long l;      // File offset of left child node
	long r;      // File offset of right child node

	BSTNode()
	{
		key = -1;
		l = r = -1;
	}

	BSTNode(int key)
	{	  
		this->key = key;
		l = r = -1;
	}
}BSTNode;

typedef struct BSTNodeOffset
{
	BSTNode node;
	long offset;

	BSTNodeOffset(BSTNode node, long offset)
	{
		this->node = node;
		this->offset = offset;
	}
}BSTNodeOffset;

typedef enum ChildType
{
	Left,
	Right
}ChildType;

class BSTManager
{
private:
	filereader *file_handler;

	BSTNode Deserialize(long offset);
	void Serialize(BSTNode node, long offset);

	long FindParentOffset(int new_key);
	bool UpdateParent(long parent_offset, 
		long child_offset, ChildType child_type);

public:
	BSTManager(string index_filepath);
	~BSTManager();
	void Add(int key);
	bool Find(int key);
	void End();
	void Print();
};