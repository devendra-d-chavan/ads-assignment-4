#include <queue>
#include <exception>

#include "bst.h"

using std::cout;
using std::endl;
void swap_queue(std::queue<BSTNodeOffset> **current_level, std::queue<BSTNodeOffset> **next_level);

BSTManager::BSTManager(string index_filepath)
{
	this->file_handler = new filereader();

	// Clear the previous contents
	this->file_handler->open(index_filepath, 'w', 1, 1);
	this->file_handler->close();

	this->file_handler->open(index_filepath,'x', 1, 1);	
}

BSTManager::~BSTManager()
{
	this->End();
}

void BSTManager::End()
{
	if(this->file_handler != NULL)
	{
		this->file_handler->close();
		this->file_handler = NULL;
	}
}

void BSTManager::Print()
{
	std::queue<BSTNodeOffset> *current_level = new std::queue<BSTNodeOffset>();
	std::queue<BSTNodeOffset> *next_level = new std::queue<BSTNodeOffset>();

	this->file_handler->seek(0, seek_mode::BEGIN);

	BSTNode node;
	if(this->file_handler->read_raw((char*)&node, sizeof(node)) < sizeof(node))
	{
		// File is empty
		this->file_handler->close();
		return;
	}

	current_level->push(BSTNodeOffset(node, 0));
	long offset = 0;
	int level = 1;
	cout << level++ << ": ";

	while(!current_level->empty())
	{
		BSTNodeOffset parent_node = current_level->front();
		current_level->pop();

		std::cout << parent_node.node.key << "/" 
			<< parent_node.offset << " ";

		if(parent_node.node.l != -1)
		{
			BSTNode left_child;
			this->file_handler->seek(parent_node.node.l, 
				seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&left_child, 
				sizeof(left_child));
			next_level->push(BSTNodeOffset(left_child, parent_node.node.l));
		}

		if(parent_node.node.r != -1)
		{
			BSTNode right_child;
			this->file_handler->seek(parent_node.node.r, 
				seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&right_child, 
				sizeof(right_child));
			next_level->push(BSTNodeOffset(right_child, parent_node.node.r));
		}

		if(current_level->empty())
		{
			cout << endl;
			swap_queue(&current_level, &next_level);

			if(!current_level->empty() || !next_level->empty())
			{
				cout << level++ << ": ";
			}
		}
	}

	this->file_handler->close();
}

void BSTManager::Add(int key)
{
	BSTNode child_node(key);

	long parent_offset = this->FindParentOffset(key);
	if(parent_offset == -1)
	{
		// Parent does not exist i.e. no node in the tree
		this->Serialize(child_node, 0);
		return;
	}

	// Get the new offset (end of file)
	this->file_handler->seek(0, seek_mode::END);
	long new_offset = this->file_handler->offset();

	// Write the new child
	this->Serialize(child_node, new_offset);

	// Determine whether to add as a left or right child
	// Read the node at parent offset
	BSTNode parent_node = this->Deserialize(parent_offset);
	if(key < parent_node.key)
	{
		parent_node.l = new_offset;
	}
	else
	{
		parent_node.r = new_offset;
	}

	// Update the parent to child pointer
	this->UpdateParent(parent_offset, new_offset, 
		key < parent_node.key ? 
		ChildType::Left : ChildType::Right);
}

bool BSTManager::Find(int key)
{
	this->file_handler->seek(0, seek_mode::BEGIN);

	BSTNode node;	
	if(this->file_handler->read_raw((char*)&node, sizeof(BSTNode)) !=
		sizeof(node))
	{
		// Unable to find root, file is empty
		return false;
	}

	long child_offset = 0;
	while(child_offset != -1)
	{		
		if(key < node.key)
		{
			child_offset = node.l;
		}
		else if(key > node.key)
		{
			child_offset = node.r;
		}
		else
		{
			return true;
		}

		if(child_offset != -1)
		{
			this->file_handler->seek(child_offset, seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&node, sizeof(node));
		}
	}	

	return false;
}

void BSTManager::Serialize(BSTNode node, long offset)
{
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->write_raw((char*)&node, sizeof(node));
}

BSTNode BSTManager::Deserialize(long offset)
{
	// Check if the file is empty
	this->file_handler->seek(offset, seek_mode::END);
	if(this->file_handler->offset() == 0)
	{
		throw std::runtime_error("Empty file");
	}

	BSTNode node;
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->read_raw((char*)&node, sizeof(BSTNode));

	return node;
}

bool BSTManager::UpdateParent(long parent_offset, 
							   long child_offset, 
							   ChildType child_type)
{
	BSTNode node = this->Deserialize(parent_offset);
	if(child_type == ChildType::Left)
	{
		node.l = child_offset;
	}
	else
	{
		node.r = child_offset;
	}

	this->Serialize(node, parent_offset);
	return true;
}

long BSTManager::FindParentOffset(int key)
{
	long parent_offset = 0;
	this->file_handler->seek(parent_offset, seek_mode::BEGIN);

	BSTNode node;
	try
	{
		node = this->Deserialize(parent_offset);	
	}
	catch(std::runtime_error err)
	{
		// Unable to find node
		return -1;
	}

	long child_offset = 0;
	while(child_offset != -1)
	{		
		parent_offset = child_offset;
		if(key < node.key)
		{
			child_offset = node.l;
		}
		else
		{
			child_offset = node.r;
		}

		if(child_offset != -1)
		{
			this->file_handler->seek(child_offset, seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&node, sizeof(node));
		}
	}	

	return parent_offset;
}

void swap_queue(std::queue<BSTNodeOffset> **current_level, std::queue<BSTNodeOffset> **next_level)
{
	std::queue<BSTNodeOffset> *temp = *current_level;
	*current_level = *next_level;
	*next_level = temp;
}