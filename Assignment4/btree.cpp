#include <queue>
#include <exception>

#include "btree.h"

using std::cout;
using std::endl;
void swap_queue(std::queue<BTreeNodeOffset> **current_level, std::queue<BTreeNodeOffset> **next_level);

BTreeManager::BTreeManager(string index_filepath)
{
	this->file_handler = new filereader();

	this->current_root_offset = 0;

	// Clear the previous contents
	this->file_handler->open(index_filepath, 'w', 1, 1);
	this->file_handler->close();

	this->file_handler->open(index_filepath,'x', 1, 1);	
}

BTreeManager::~BTreeManager()
{
	this->End();
}

void BTreeManager::End()
{
	if(this->file_handler != NULL)
	{
		this->file_handler->close();
		this->file_handler = NULL;
	}
}

void BTreeManager::Print()
{
	std::queue<BTreeNodeOffset> *current_level = new std::queue<BTreeNodeOffset>();
	std::queue<BTreeNodeOffset> *next_level = new std::queue<BTreeNodeOffset>();

	this->file_handler->seek(this->current_root_offset, seek_mode::BEGIN);

	BTreeNode node;
	if(this->file_handler->read_raw((char*)&node, sizeof(node)) < sizeof(node))
	{
		// File is empty
		this->file_handler->close();
		return;
	}

	current_level->push(BTreeNodeOffset(node, this->current_root_offset));
	long offset = 0;
	int level = 1;
	cout << level++ << ": ";

	while(!current_level->empty())
	{
		BTreeNodeOffset parent_node = current_level->front();
		current_level->pop();

		for(int i = 0; i < parent_node.node.n; i++)
		{
			std::cout << parent_node.node.key[i];
			if(i != parent_node.node.n - 1)
			{
				std::cout << ",";
			}
		}
		std::cout << "/" << parent_node.offset << " ";

		for(int i = 0; i < parent_node.node.n + 1; i++)
		{
			if(parent_node.node.child[i] != -1)
			{
				BTreeNode child_node;
				this->file_handler->seek(parent_node.node.child[i], 
											seek_mode::BEGIN);
				this->file_handler->read_raw((char*)&child_node, 
												sizeof(child_node));
				next_level->push(BTreeNodeOffset(child_node, 
									parent_node.node.child[i]));
			}
		}

		if(current_level->empty())
		{
			cout << endl;
			swap_queue(&current_level, &next_level);

			if(!current_level->empty() || !next_level->empty())
			{
				cout << level++ << ": ";
			}
		}
	}

	this->file_handler->close();
}

void BTreeManager::Add(int key)
{
	BTreeNode current_node;
	long leaf_node_offset = this->GetLeafOffset(key);
	if(leaf_node_offset == -1)
	{		
		// Empty file
		current_node.key[0] = key;
		current_node.n += 1;
		this->Serialize(current_node, 0);
		return;
	}

	current_node = this->Deserialize(leaf_node_offset);
	
	this->SplitPromote(current_node, key, leaf_node_offset, -1, -1);

	if(this->Find(key) == false)
	{
		cout << "Error";
	}
}

void BTreeManager::SplitPromote(BTreeNode current_node, int key, 
								 long current_node_offset,
								 long offset_left,
								 long offset_right)
{
	// Check if the current node is full
	if(current_node.n == BTREE_ORDER)
	{
		BTreeNode new_node;
		// Add the new node to the end of the file
		this->file_handler->seek(0, seek_mode::END);
		long new_offset = this->file_handler->offset();

		int promoted_key = this->SplitChild(&current_node, &new_node, key);
		
		if(promoted_key != key)
		{
			// Find which node the new key was inserted
			int i = 0;
			while(i < current_node.n)
			{
				if(current_node.key[i] == key)
				{
					current_node.child[i] = offset_left;
					current_node.child[i+1] = offset_right;
					break;
				}

				i++;
			}

			i = 0;
			while(i < new_node.n)
			{
				if(new_node.key[i] == key)
				{
					new_node.child[i] = offset_left;
					new_node.child[i+1] = offset_right;
					break;
				}

				i++;
			}
		}

		this->Serialize(current_node, current_node_offset);
		this->Serialize(new_node, new_offset);

		long parent_offset = this->GetParentOffset(current_node_offset, key);

		if(parent_offset == -1)
		{
			// Add the new root to the end of the file
			this->file_handler->seek(0, seek_mode::END);
			this->current_root_offset = this->file_handler->offset();

			BTreeNode root_node;
			root_node.key[0] = promoted_key;
			root_node.child[0] = current_node_offset;
			root_node.child[1] = new_offset;
			root_node.n += 1;
			this->Serialize(root_node, this->current_root_offset);	
		}
		else
		{
			BTreeNode parent_node = this->Deserialize(parent_offset);
			this->SplitPromote(parent_node, promoted_key, parent_offset, current_node_offset, new_offset);
		}
	}
	else
	{
		// Insert into the current node
		int i = current_node.n - 1;
		// Find the new location to insert
		while (i >= 0 && current_node.key[i] > key)
		{
			current_node.key[i+1] = current_node.key[i];

			current_node.child[i+2] = current_node.child[i+1];
			current_node.child[i+1] = current_node.child[i];

			i--;
		}
 
		// Insert the new key at found location
		current_node.key[i+1] = key;

		current_node.child[i+1] = offset_left;
		current_node.child[i+2] = offset_right;

		current_node.n += 1;
		
		this->Serialize(current_node, current_node_offset);	
	}
}

long BTreeManager::GetParentOffset(long offset, int key)
{
	long parent_offset = this->current_root_offset;
	BTreeNode node = this->Deserialize(parent_offset);

	long child_offset = this->current_root_offset;
	while(child_offset != -1)
	{		
		parent_offset = child_offset;

		// Find the next child
		bool is_right_most_child = true;
		for(int i = 0; i < node.n; i++)
		{
			if(key < node.key[i])
			{
				child_offset = node.child[i];
				is_right_most_child = false;
				break;
			}
		}

		if(is_right_most_child)
		{
			child_offset = node.child[node.n];
		}

		if(child_offset == offset)
		{
			return parent_offset;
		}
		
		if(child_offset != -1)
		{
			this->file_handler->seek(child_offset, seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&node, sizeof(node));
		}
	}	

	return -1;
}

int BTreeManager::SplitChild(BTreeNode *current_node, BTreeNode *new_node, int key)
{
	// Find the location to insert
	int insert_index = 0;
	while(insert_index < current_node->n && 
			current_node->key[insert_index] < key)
	{
		insert_index++;
	}

	int promoted_key;
	int second_half_start_index;
	if(insert_index < BTREE_ORDER/2)
	{
		// Key is to be inserted into the first half
		second_half_start_index = BTREE_ORDER/2;
		promoted_key = current_node->key[BTREE_ORDER/2 - 1];
	}
	else if(insert_index > BTREE_ORDER/2)
	{
		// Key is to be inserted into the second half
		second_half_start_index = BTREE_ORDER/2 + 1;
		promoted_key = current_node->key[BTREE_ORDER/2];
	}
	else if(insert_index == BTREE_ORDER/2)
	{
		second_half_start_index = BTREE_ORDER/2;
		promoted_key = key;
	}

	// Copy the second half of the keys to the new node
	for(int i = second_half_start_index, j = 0; i < BTREE_ORDER; i++, j++)
	{
		new_node->key[j] = current_node->key[i];
	}

	// Copy the second half of the child links to the new node
	for(int i = second_half_start_index, j = 0; i < BTREE_ORDER + 1; i++, j++)
	{
		new_node->child[j] = current_node->child[i];
	}

	if(promoted_key == key) 
	{
		// The new key is being promoted, 
		// therefore insertion in new_node/current_node is not required.
		current_node->n = BTREE_ORDER/2;
		new_node->n = BTREE_ORDER/2;
		return promoted_key;
	}

	if(key < promoted_key)
	{
		// Key in first half		
		current_node->n = BTREE_ORDER/2 - 1;
		new_node->n = BTREE_ORDER/2;

		int i = current_node->n - 1;
		// Find the new location to insert
		while (i >= 0 && current_node->key[i] > key)
		{
			current_node->key[i+1] = current_node->key[i];
			current_node->child[i+2] = current_node->child[i+1];
			current_node->child[i+1] = current_node->child[i];
			i--;
		}
 
		// Insert the new key at found location
		current_node->key[i+1] = key;		
		current_node->n += 1;
	}
	else
	{
		current_node->n = BTREE_ORDER/2;
		new_node->n = BTREE_ORDER/2 - 1;

		int i = new_node->n - 1;
		// Find the new location to insert
		while (i >= 0 && new_node->key[i] > key)
		{
			new_node->key[i+1] = new_node->key[i];
			new_node->child[i+2] = new_node->child[i+1];
			new_node->child[i+1] = new_node->child[i];
			i--;
		}
 
		// Insert the new key at found location
		new_node->key[i+1] = key;
		new_node->n += 1;
	}

	return promoted_key;
}

bool BTreeManager::Find(int key)
{
	this->file_handler->seek(this->current_root_offset, seek_mode::BEGIN);

	BTreeNode node;
	try
	{
		node = this->Deserialize(this->current_root_offset);	
	}
	catch(std::runtime_error err)
	{
		// Unable to find node
		return false;
	}

	long child_offset = this->current_root_offset;
	while(child_offset != -1)
	{		
		// Find the next child
		bool is_right_most_child = true;
		for(int i = 0; i < node.n; i++)
		{
			if(key == node.key[i])
			{
				return true;
			}

			if(key < node.key[i])
			{
				child_offset = node.child[i];
				is_right_most_child = false;
				break;
			}
		}

		if(is_right_most_child)
		{
			child_offset = node.child[node.n];
		}
		
		if(child_offset != -1)
		{
			this->file_handler->seek(child_offset, seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&node, sizeof(node));
		}
	}	

	return false;
}

void BTreeManager::Serialize(BTreeNode node, long offset)
{
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->write_raw((char*)&node, sizeof(node));
}

BTreeNode BTreeManager::Deserialize(long offset)
{
	// Check if the file is empty
	this->file_handler->seek(offset, seek_mode::END);
	if(this->file_handler->offset() == 0)
	{
		throw std::runtime_error("Empty file");
	}

	BTreeNode node;
	this->file_handler->seek(offset, seek_mode::BEGIN);
	this->file_handler->read_raw((char*)&node, sizeof(BTreeNode));

	return node;
}

long BTreeManager::GetLeafOffset(int key)
{
	long leaf_offset = this->current_root_offset;
	this->file_handler->seek(leaf_offset, seek_mode::BEGIN);

	BTreeNode node;
	try
	{
		node = this->Deserialize(leaf_offset);	
	}
	catch(std::runtime_error err)
	{
		// Unable to find node
		return -1;
	}

	long child_offset = this->current_root_offset;
	while(child_offset != -1)
	{		
		leaf_offset = child_offset;

		// Find the next child
		bool is_right_most_child = true;
		for(int i = 0; i < node.n; i++)
		{
			if(key < node.key[i])
			{
				child_offset = node.child[i];
				is_right_most_child = false;
				break;
			}
		}

		if(is_right_most_child)
		{
			child_offset = node.child[node.n];
		}
		
		if(child_offset != -1)
		{
			this->file_handler->seek(child_offset, seek_mode::BEGIN);
			this->file_handler->read_raw((char*)&node, sizeof(node));
		}
	}	

	return leaf_offset;
}


void swap_queue(std::queue<BTreeNodeOffset> **current_level, std::queue<BTreeNodeOffset> **next_level)
{
	std::queue<BTreeNodeOffset> *temp = *current_level;
	*current_level = *next_level;
	*next_level = temp;
}