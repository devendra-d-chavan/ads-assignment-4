#include <string>
#include <iostream>
#include <iomanip>  
#include <windows.h>
#include <ctime>

#include "bst.h"
#include "btree.h"

#define BST_EXECUTE true

#define MAX_USER_INPUT_LEN 200
#define MAX_USER_INPUT_TOKENS 2

using std::cin;
using std::cout;
using std::endl;

int bst(int argc, char *argv[]);
int btree(int argc, char *argv[]);

int main( int argc, char *argv[] )
{
	if(BST_EXECUTE)
	{
		bst(argc, argv);
	}
	else
	{
		btree(argc, argv);
	}

	return 0;
}

int bst(int argc, char *argv[])
{
	clock_t cumulative_ticks = 0;
	clock_t start_ticks, end_ticks;
	size_t find_count = 0;

	if(argc != 2)
	{
		cout << "Program usage : bst <index filename>. For example, bst index.bst"
			<< endl;

		return 1;
	}

	string index_name = (char*)argv[1];	

	string buf[MAX_USER_INPUT_TOKENS];
	bool end;
	char user_input[MAX_USER_INPUT_LEN];

	BSTManager *bst_manager = new BSTManager(index_name);

	do
	{
		end = false;
		cin.getline(user_input, MAX_USER_INPUT_LEN);
		string input = user_input;

		input.token(buf, MAX_USER_INPUT_TOKENS);

		string command = buf[0];

		int argument = 0;
		if(command != "end" || command != "print")
		{
			argument = atoi(buf[1]);
		}

		if(command == "add")
		{
			bst_manager->Add(argument);
		}
		else if(command == "find")
		{
			start_ticks = clock();
			bool found;
			found = bst_manager->Find(argument);

			end_ticks = clock();

			cumulative_ticks += end_ticks - start_ticks;
			find_count++;

			if(found)
			{
				cout << "Record " << argument << " exists." << endl;
			}
			else
			{
				cout << "Record " << argument << " does not exist." << endl;
			}
		}
		else if(command == "end")
		{
			bst_manager->End();
			end = true;
		}
		else if(command == "print")
		{
			cout << endl;
			bst_manager->Print();

			cout << endl;
			cout << "Sum: " << std::fixed << std::setprecision(6) << ((float)cumulative_ticks)/CLOCKS_PER_SEC << endl;
			cout << "Avg: " << std::fixed << std::setprecision(6) << ((float)cumulative_ticks)/(CLOCKS_PER_SEC * find_count) << endl;
		}
		else
		{
			cout << "Invalid input";
		}
	}while(!end);

	return 0;
}

int btree(int argc, char *argv[])
{
	clock_t cumulative_ticks = 0;
	clock_t start_ticks, end_ticks;
	size_t find_count = 0;

	if(argc != 2)
	{
		cout << "Program usage : btree <index filename>. For example, btree index.btree"
			<< endl;

		return 1;
	}

	string index_name = (char*)argv[1];	

	string buf[MAX_USER_INPUT_TOKENS];
	bool end;
	char user_input[MAX_USER_INPUT_LEN];

	BTreeManager *btree_manager = new BTreeManager(index_name);

	do
	{
		end = false;
		cin.getline(user_input, MAX_USER_INPUT_LEN);
		string input = user_input;

		input.token(buf, MAX_USER_INPUT_TOKENS);

		string command = buf[0];

		int argument = 0;
		if(command != "end" || command != "print")
		{
			argument = atoi(buf[1]);
		}

		if(command == "add")
		{
			btree_manager->Add(argument);
		}
		else if(command == "find")
		{
			start_ticks = clock();
			bool found;
			found = btree_manager->Find(argument);

			end_ticks = clock();

			cumulative_ticks += end_ticks - start_ticks;
			find_count++;

			if(found)
			{
				cout << "Record " << argument << " exists." << endl;
			}
			else
			{
				cout << "Record " << argument << " does not exist." << endl;
			}
		}
		else if(command == "end")
		{
			btree_manager->End();
			end = true;
		}
		else if(command == "print")
		{
			cout << endl;
			btree_manager->Print();

			cout << endl;
			cout << "Sum: " << std::fixed << std::setprecision(6) << ((float)cumulative_ticks)/CLOCKS_PER_SEC << endl;
			cout << "Avg: " << std::fixed << std::setprecision(6) << ((float)cumulative_ticks)/(CLOCKS_PER_SEC * find_count) << endl;
		}
		else
		{
			cout << "Invalid input";
		}
	}while(!end);

	return 0;
}