#include "filereader.h"
#define BTREE_ORDER 32

typedef struct BTreeNode {
	int n;						 // Number of keys in node
	int key[BTREE_ORDER];        // Key values
	long child[BTREE_ORDER+1];   // File offsets of child nodes
	
	BTreeNode()
	{
		n = 0;
		for(int i = 0; i < BTREE_ORDER; i++)
		{
			key[i] = -1;
			child[i] = -1;
		}

		child[BTREE_ORDER] = -1;
	}
	
}BTreeNode;

typedef struct BTreeNodeOffset
{
	BTreeNode node;
	long offset;

	BTreeNodeOffset(BTreeNode node, long offset)
	{
		this->node = node;
		this->offset = offset;
	}
}BTreeNodeOffset;

class BTreeManager
{
private:
	filereader *file_handler;
	long current_root_offset;

	BTreeNode Deserialize(long offset);
	void Serialize(BTreeNode node, long offset);

	long GetLeafOffset(int new_key);
	long BTreeManager::GetParentOffset(long child_offset, int key);

	int SplitChild(BTreeNode *current_node, BTreeNode *new_node, int key);
	void SplitPromote(BTreeNode current_node, int key, 
								 long current_node_offset,
								 long offset_left,
								 long offset_right);

public:
	BTreeManager(string index_filepath);
	~BTreeManager();
	void Add(int key);
	bool Find(int key);
	void End();
	void Print();
};